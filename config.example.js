exports.token = 'Bot ToKeN'
exports.toggleCmd = 'c!channel'
exports.status = 'c!channel'

exports.replaceMap = {
  '0️⃣': '0',
  '🇴':' 0',
  '🅾️': '0',
  '⭕': '0',
  '1️⃣': '1',
  '🇮': '1',
  '2️⃣': '2',
  '3️⃣': '3',
  '4️⃣': '4',
  '5️⃣': '5',
  '6️⃣': '6',
  '7️⃣': '7',
  '8️⃣': '8',
  '9️⃣': '9',
  '🔟': '10',
  '🔢': '1234',
  '💯': '100',
  'ℹ️': 'i',
  '♾️': 'Infinity',
  '❗': '!',
  '❕': '!',
  '‼️': '!!',
  '✴️': '\\*',
  '*️⃣': '\\*',
  '❎': '\\*',
  '✳️': '\\*',
  '❇️': '\\*',
}
exports.intReplaceMap = /, _/g
exports.managerRoles = []
exports.ruinTemplates = [
  'RUINED IT AT **$X**!!',
  'SO CLOSE, **$X** WAS NOT IT!!',
  'GOT RUINED AGAIN AT **$X**!!',
  'YOU RUINED IT AT **$X**!!',
]
exports.ruinComments = [
  'Who told you to ruin the count?',
  'Why are you doing this on purpose.',
  'I’m tired of telling you this.',
]
