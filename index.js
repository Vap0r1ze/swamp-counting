const Discord = require('eris')
const chalk = require('chalk')
const math = require('mathjs')
const db = require('quick.db')
const format = require('date-fns/format')
const config = require('./config')

const client = new Discord.Client(config.token, {
  intents: [
    'guilds',
    'guildMessages',
  ],
})
const getFormattedNow = () => format(Date.now(), '[hh:mm:ss aa]')
const getArchiveEmbed = (channelData, reason) => ({
  color: 0xE84144,
  description: `**${channelData.count}**`,
  author: {
    icon_url: channelData.lastUser.avatarURL,
    name: `${channelData.lastUser.username}#${channelData.lastUser.discriminator}`,
  },
  footer: {
    text: `This is a repost of the current number because ${reason}`,
  },
})
const parseNum = text => {
  const formattedText = text.replace(new RegExp(Object.keys(config.replaceMap).map(k => k.split('').map(c => '\\u' + c.charCodeAt().toString(16).padStart(4, 0)).join('')).join('|'), 'g'), m => config.replaceMap[m])
  let msgNum
  try {
    msgNum = math.evaluate(formattedText)
    switch (math.typeOf(msgNum)) {
      case 'Unit':
        msgNum = msgNum.value
        break
      case 'Complex':
        msgNum = msgNum.re
        break
    }
  } catch {}
  if ((typeof msgNum !== 'number' && typeof msgNum !== 'string') || Number.isNaN(+msgNum)) {
    msgNum = parseInt(formattedText.replace(config.intReplaceMap, ''), 10)
  }
  return +msgNum
}

client.on('ready', () => {
  const fNow = getFormattedNow()
  const channelIds = db.all().map(r => r.ID)
  console.log(fNow, chalk`Logged into {cyan.bold ${client.user.username}#${client.user.discriminator}}`)
  console.log(fNow, chalk`Loaded {green.bold ${channelIds.length}} counting channels from storage`)
  for (const channelId of channelIds) {
    const channelData = db.get(channelId)
    const channelLog = [chalk`{bgGreen.bold  ${channelId} }`]
    const guild = client.channelGuildMap[channelId]
    if (guild)
      channelLog.push(chalk`{bgCyan.bold  #${client.guilds.get(guild).channels.get(channelId).name} }`)
    console.log(fNow, ...channelLog, chalk`Count initialized at {green.bold ${channelData.count}}`)
  }
  client.editStatus({ name: config.status })
})
client.on('messageCreate', async message => {
  if (!(message.channel instanceof Discord.TextChannel)) return
  if (message.author.id === client.user.id) return
  if (message.author.bot || !message.member) return

  if (message.content.split(' ')[0] === config.toggleCmd) {
    if (
      !message.member.roles.some(r => config.managerRoles.includes(r.id))
      && !message.member.permissions.has('manageGuild')
      && !message.channel.permissionsOf(message.member).has('manageChannels')
    ) return
    const channelData = db.get(message.channel.id)
    if (channelData) db.delete(message.channel.id)
    else db.set(message.channel.id, {
      lastUser: null,
      lastMsg: null,
      count: 0,
    })
    return message.reply(`${channelData ? '❎' : '✅'}  **|** Counting has been **${channelData ? 'dis' : 'en'}abled** for this channel.`)
  }

  let forceRight = false
  let msgContent = message.content
  if (message.member.permissions.has('administrator')) {
    if (msgContent.startsWith('> ')) {
      msgContent = msgContent.slice(2)
      forceRight = true
    }
  }

  const msgNum = parseNum(msgContent)
  if (!Number.isNaN(msgNum) && msgNum != null) {
    const channelData = db.get(message.channel.id)
    if (!channelData) return
    if (channelData.lastUser?.id === message.author.id && !forceRight) return

    let wrongNum = !forceRight && msgNum !== channelData.count + 1

    if (wrongNum) {
      if (channelData.count === 0) return
      const oldCount = channelData.count
      channelData.count = 0
      channelData.lastUser = null
      channelData.lastMsg = null
      db.set(message.channel.id, channelData)
      await message.addReaction('❌')
      const ruinTemplate = config.ruinTemplates[Math.floor(Math.random() * config.ruinTemplates.length)]
      const ruinComment = config.ruinComments[Math.floor(Math.random() * config.ruinComments.length)]
      return message.channel.createMessage({
        content: `${ruinTemplate.replace('$X', msgNum).replace('$Y', oldCount)} Count has reset, next number is 1️⃣.\n*${ruinComment}*`,
        messageReference: { messageID: message.id },
      })
    } else { // Right num
      channelData.count = msgNum
      channelData.lastUser = {
        id: message.author.id,
        username: message.author.username,
        discriminator: message.author.discriminator,
        avatarURL: message.author.dynamicAvatarURL('png', 128),
      }
      channelData.lastMsg = { id: message.id }
      db.set(message.channel.id, channelData)
      await message.addReaction('✅')
      return
    }
  }
})
client.on('messageUpdate', message => {
  const channelData = db.get(message.channel.id)
  if (channelData?.lastMsg?.id !== message.id) return

  const msgNum = parseNum(message.content)
  if (msgNum !== channelData.count) {
    message.channel.createMessage({
      embeds: [getArchiveEmbed(channelData, 'it was edited')],
      messageReference: { messageID: message.id },
    })
  }
})
client.on('messageDelete', message => {
  const channelData = db.get(message.channel.id)
  if (channelData?.lastMsg?.id !== message.id) return
  if (!(message.channel instanceof Discord.TextChannel)) return
  message.channel.createMessage({
    embeds: [getArchiveEmbed(channelData, 'it was deleted')]
  })
})

client.connect()
